/*
 Controlling a servo position using a Joystick Module (variable resistor)
 By Lionel Tellem
*/

#include <Servo.h>
const int SW_pin = 2; // digital pin connected to switch output
const int X_pin = A0; // analog pin connected to X output
const int Y_pin = A1; // analog pin connected to Y output

Servo myservo;  // create servo object to control a servo

int val;    // variable to read the value from the analog pin(Y_pin)

void setup() {
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object
}

void loop() {
  val = analogRead(Y_pin);            // reads the value of the Y_pin (value between 0 and 1023)
  val = map(val, 0, 1023, 0, 180);     // scale it to use it with the servo (value between 0 and 180)
  myservo.write(val);                  // sets the servo position according to the scaled value
  delay(15);                           // waits for the servo to get there
}

