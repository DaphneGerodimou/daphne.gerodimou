


import processing.serial.*; //import the Serial library
Serial myPort;

int x; // variable holding the value from A0
int y; // variable holding the value from A1
int b; // variable holding the value from digital pin 2
PFont f; // define the font variable
String portName;
String val;
//PImage me;
Rect a;
Rect a1;
Rect a2;
 
void setup()

{
size ( 900, 600 ) ; // window size
 // me = loadImage("me.png");
  //imageMode (CENTER);   
  
    // we are opening the port
   myPort = new Serial(this, "/dev/cu.usbmodem146201", 9600);
  
   myPort.readStringUntil('\n'); 

a = new Rect(color(0,0,255), 100, 0, 2, 100, 100);
a1 = new Rect(color(0,255,0), 400, 0, 5, 50, 50);
a2 = new Rect(color(255,0,0), 650, 0, 3, 150, 150);
  

   

}

  // choose the font and size
 // f = createFont("Arial", 16, true); // Arial, 16px, anti-aliasing
 // textFont ( f, 16 ) ; // size 16px
//}

// drawing loop
void draw()
{
  background(0);
  
a.ascend();
a.display();
//a.top();

a1.ascend();
a1.display();
a2.ascend();
a2.display();
  

  if (b == 1) // check if the button is pressed
  {
    // draw a larger circle with specified coordinates
    fill(255,255,0);
    ellipse( x, y, 50, 50);
  } 
  else
  {
    // we draw a circle with a certain coordinates
    fill(255,255,0);
   ellipse( x, y, 20, 20);
  }
  
 if (a.MouseIsOver()==true) {
    
    a = new Rect(color(0,0,255),100, 0, 2, 100, 100) ;}
   else if (a1.MouseIsOver()==true) {
    a1 =new  Rect(color(0,255,0),400, 0, 5, 50, 50); }
    else if (a2.MouseIsOver()==true) {
    a2 = new Rect(color(255,0,0), 650, 0, 3, 150, 150);  
    }

if (a.OverY () == true) {
 // background(0,0,255);
  myPort.write('y');
  
  }
if  (a1.OverY () == true) {
 // background(0,255,0); 
  myPort.write('r');
 }
 if 
  (a2.OverY () == true) {
 // background(255,0,0);
myPort.write('b');}

  else {myPort.write('f');}
  
  // we display data
  text("AnalogX="+(1023-x)+" AnalogY="+(1023-y),10,20);
}




// data support from the serial port
void serialEvent( Serial myPort) 
{
  // read the data until the newline n appears
  val = myPort.readStringUntil('\n');
  
  if (val != null)
  {
        val = trim(val);
        
    // break up the decimal and new line reading
    int[] vals = int(splitTokens(val, ","));
    
    // we assign to variables
    x = vals[0];
    y = vals[1] ;
    b = vals[2];

  }
}
