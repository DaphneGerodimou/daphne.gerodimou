
//arduino pin numbers 
const int SV_pin = 2; //digital pin connected to switch output
const int X_pin = 0;  //analog pin connected to X output
const int Y_pin = 1;  //analog pin connected to Y output 

void setup() {
  pinMode(SV_pin, INPUT); 
  digitalWrite(SV_pin, HIGH);
  Serial.begin(115200);

}

void loop() {
 Serial.print("Svitch:  ");
 Serial.print(digitalRead(SV_pin));
 Serial.print("\n");
 Serial.print("X-axis: ");
 Serial.print(analogRead(X_pin));
 Serial.print("\n");
 Serial.print("Y-axis: ");
 Serial.println(analogRead(Y_pin));
 Serial.print("\n\n"); 
 delay(500);

}
